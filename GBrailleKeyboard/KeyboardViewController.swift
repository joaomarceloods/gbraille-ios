//
//  KeyboardViewController.swift
//  Keyboard Demo
//
//  Created by João Marcelo on 25/09/15.
//  Copyright © 2015 João Marcelo Souza. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

class KeyboardViewController: UIInputViewController {
    
    
    
    
    
    // MARK: - UI View Controller
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadKeyboardView()
    }
    
    override func viewDidAppear(animated: Bool) {
        self.initViewConstraints()
        self.initAudioPlayers()
        self.speak(self.OPENED_KEYBOARD)
        self.speak(self.INSTRUCTIONS_SPEECH, ifUserDefaultsKeyIsFalse: "instructions_speech")
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.speak(self.CLOSED_KEYBOARD)
    }
    
    
    
    
    
    // MARK: - Keyboard View Controller
    
    // MARK: Constants
    
    let OPENED_KEYBOARD = "iBraille"
    let CLOSED_KEYBOARD = "Fechou iBraille"
    let UNKNOWN_CHARACTER_SPEECH = "O sinal que você tentou usar não existe ou não foi reconhecido. Quando isto acontecer, o dispositivo vibrará."
    let NO_TEXT_TO_DELETE = "Não há mais texto para apagar. Quando isto acontecer, o dispositivo vibrará."
    let UNKNOWN_CHARACTER_DISPLAY = "?"
    let INCOMPLETE_CHARACTER_DISPLAY = "…"
    let SPACE_CHARACTER_SPEECH = "Sinal de espaço"
    let NUMBER_SPEECH = "Número"
    let LOWERCASE_SPEECH = "Minúscula"
    let UPPERCASE_SPEECH = "Maiúscula"
    let UPPERCASE_WORD_SPEECH = "Palavra maiúscula"
    let LAST_WORD = "Último trecho"
    let ALL_WORDS = "Texto completo"
    let NO_TEXT = "Nenhum texto restante"
    let CANCELLED_TYPING = "Cancelou"
    let INSTRUCTIONS_SPEECH = "Olá, você está usando o teclado iBraille... O teclado preenche três quartos da sua tela, começando na parte de baixo... Instruções... Insira os pontos linha por linha... Para fazer um ponto no lado esquerdo, toque do lado esquerdo... Para fazer um ponto do lado direito, toque do lado direito... Para fazer dois pontos, toque com dois dedos ao mesmo tempo... Para fazer uma linha vazia, deslize da esquerda para a direita... Quando você preencher três linhas, o caractere correspondente será adicionado... Para ouvir o último texto digitado, pressione por um segundo... Para ouvir todo o texto já digitado, pressione por um segundo com dois dedos... Para cancelar a digitação, ou apagar um caractere, deslize da direita para a esquerda... A posição dos toques não influencia a digitação, mas lembre-se que o teclado ocupa apenas três quartos da tela, então recomenda-se usar a metade inferior da tela... Agora, pode começar a digitar... Para dispensar o teclado, toque com três dedos ao mesmo tempo."
    
    let SPEECH_SYNTHESIZER = AVSpeechSynthesizer()
    
    
    
    
    
    // MARK: Variables
    
    var audioPlayer_Tap: AVAudioPlayer?
    var audioPlayer_TapDouble: AVAudioPlayer?
    var audioPlayer_SwipeLeft: AVAudioPlayer?
    var audioPlayer_SwipeRight: AVAudioPlayer?
    
    var number = false
    var uppercase = false
    var uppercaseWord = false
    
    var brailleRow = 0
    var brailleCell = [
        0,0,
        0,0,
        0,0]
    
    
    
    
    
    // MARK: Methods
    
    // Carrega a view a partir do .xib e sobrescreve a view deste controller
    func loadKeyboardView() {
        let nib = UINib(nibName: "KeyboardView", bundle: nil)
        let keyboardView = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.view = keyboardView
        self.setDisplayText("")
    }
    
    // Cria uma constraint de altura
    func initViewConstraints() {
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        let heightConstraint = NSLayoutConstraint(
            item: self.view,
            attribute: .Height,
            relatedBy: .Equal,
            toItem: nil,
            attribute: .NotAnAttribute,
            multiplier: 1,
            constant: screenHeight * 0.7)
        self.view.addConstraint(heightConstraint)
    }
    
    // Carrega os sons de interface
    func initAudioPlayers() {
        let url_audioPlayer_Tap = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("Tap", ofType: "aiff")!)
        let url_audioPlayer_TapDouble = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("TapDouble", ofType: "aiff")!)
        let url_audioPlayer_SwipeLeft = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("SwipeLeft", ofType: "aiff")!)
        let url_audioPlayer_SwipeRight = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("SwipeRight", ofType: "aiff")!)
        
        self.audioPlayer_Tap = try! AVAudioPlayer(contentsOfURL: url_audioPlayer_Tap)
        self.audioPlayer_TapDouble = try! AVAudioPlayer(contentsOfURL: url_audioPlayer_TapDouble)
        self.audioPlayer_SwipeLeft = try! AVAudioPlayer(contentsOfURL: url_audioPlayer_SwipeLeft)
        self.audioPlayer_SwipeRight = try! AVAudioPlayer(contentsOfURL: url_audioPlayer_SwipeRight)
        
        self.audioPlayer_Tap?.prepareToPlay()
        self.audioPlayer_TapDouble?.prepareToPlay()
        self.audioPlayer_SwipeLeft?.prepareToPlay()
        self.audioPlayer_SwipeRight?.prepareToPlay()
    }
    
    
    
    
    func addDotsToBrailleCell(d1 d1:Int, d2:Int) {
        
        self.playSoundForDots(d1, d2:d2)
        
        // Está começando um novo caractere?
        if self.brailleRow == 0 {
            self.setDisplayText(self.INCOMPLETE_CHARACTER_DISPLAY)
        }
        
        self.brailleCell[self.brailleRow] = d1
        self.brailleCell[self.brailleRow + 3] = d2
        self.brailleRow++
        
        // Completou um caractere?
        if self.brailleRow == 3 {
            self.readBrailleCellAndDecide(self.brailleCell)
        }
    }
    
    func playSoundForDots(d1:Int, d2:Int) {
        if d1 == 1 && d2 == 1 {
            self.audioPlayer_TapDouble?.currentTime = 0
            self.audioPlayer_TapDouble?.play()
        } else if d1 == 0 && d2 == 1 {
            self.audioPlayer_Tap?.currentTime = 0
            self.audioPlayer_Tap?.pan = 1
            self.audioPlayer_Tap?.play()
        } else if d1 == 1 && d2 == 0 {
            self.audioPlayer_Tap?.currentTime = 0
            self.audioPlayer_Tap?.pan = -1
            self.audioPlayer_Tap?.play()
        } else if d1 == 0 && d2 == 0 {
            self.audioPlayer_SwipeRight?.currentTime = 0
            self.audioPlayer_SwipeRight?.play()
        }
    }
    
    // Interpreta a entrada e decide o que fazer em seguida
    func readBrailleCellAndDecide(brailleCell: [Int]) {
        if brailleCell == BrailleCharacterMap.special["capital"]! {
            self.setUppercaseMode()
            self.resetBrailleCell()
        } else if brailleCell == BrailleCharacterMap.special["number"]! {
            self.setNumberMode()
            self.resetBrailleCell()
        } else if let character = BrailleCharacterMap.find(brailleCell, isNumber: self.number) {
            self.presentCharacterAndResetBrailleCell(character, uppercase: self.uppercase)
        } else {
            self.presentUnknownCharacterAndResetBrailleCell()
        }
    }
    
    
    
    
    
    // Acende a flag de número
    func setNumberMode() {
        self.unsetUppercaseMode(true)
        self.number = true
        self.speak(self.NUMBER_SPEECH)
    }
    
    // Desliga a flag de número
    func unsetNumberMode() {
        self.number = false
    }
    
    // Acende a flag de letra maiúscula ou, se já estiver, acende a flag de palavra maiúscula
    func setUppercaseMode() {
        self.unsetNumberMode()
        if self.uppercase {
            self.uppercaseWord = true
            self.speak(self.UPPERCASE_WORD_SPEECH)
        } else {
            self.uppercase = true;
            self.speak(self.UPPERCASE_SPEECH)
        }
    }
    
    // Desliga as flags de maiúsculas
    func unsetUppercaseMode(turnOffUpCaseWord: Bool) {
        // Desliga a flag de palavra maiúscula quando indicado
        if turnOffUpCaseWord {
            self.uppercaseWord = false
        }
        // A flag de maiúscula segue a de palavra maiúscula
        self.uppercase = self.uppercaseWord
    }
    
    
    
    
    
    // Fala e exibe o caractere reconhecido
    func presentCharacterAndResetBrailleCell(character: String, uppercase: Bool = false) {
        
        // Torna o caractere maiúsculo se necessário e fala por TTS
        let char = self.uppercase
            ? character.uppercaseString
            : character
        self.speak(char)
        
        // Se o caractere for um espaço, fala a última palavra.
        // Se o caractere for um espaço, desliga a flag de número.
        // Se o caractere for um espaço, desliga a flag de maiúscula.
        // Se o caractere NÃO for um espaço, desliga a flag de maiúscula se necessário.
        if char == " " {
            self.unsetNumberMode()
            self.unsetUppercaseMode(true)
        } else {
            self.unsetUppercaseMode(false)
        }
        
        
        self.setDisplayText(char)
        self.resetBrailleCell()
        self.insertTextToDocument(char)
    }
    
    // Fala e exibe que o caractere não foi reconhecido
    func presentUnknownCharacterAndResetBrailleCell() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate)) // vibração do sistema
        self.speak(self.UNKNOWN_CHARACTER_SPEECH, ifUserDefaultsKeyIsFalse: "unkown_character_instructions")
        self.setDisplayText(self.UNKNOWN_CHARACTER_DISPLAY)
        self.resetBrailleCell()
    }
    
    func setDisplayText(text: String) {
        self.lblDisplayText.text = text
    }
    
    func resetBrailleCell() {
        self.brailleRow = 0
        self.brailleCell = [0,0,0,0,0,0]
    }
    
    
    
    
    
    func insertTextToDocument(text: String) {
        self.textDocumentProxy.insertText(text)
    }
    
    func deleteOneCharacterFromDocument() {
        if self.textDocumentProxy.documentContextBeforeInput == nil {
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            self.speak(self.NO_TEXT_TO_DELETE, ifUserDefaultsKeyIsFalse: "no_text_to_delete")
        } else {
            self.audioPlayer_SwipeLeft?.play()
        }
        self.textDocumentProxy.deleteBackward()
    }
    
    override func advanceToNextInputMode() {
        super.advanceToNextInputMode()
    }
    
    
    
    
    
    // Fala a última palavra do texto restante por TTS
    func speakLastWord() {
        if let text = self.textDocumentProxy.documentContextBeforeInput {
            let words = text.componentsSeparatedByString(" ")
            self.speak(self.LAST_WORD)
            
            // Se a última palavra for uma string vazia, significa que há um espaço no final do texto
            self.speak((words[words.count - 1] == "")
                ? self.SPACE_CHARACTER_SPEECH
                : (words[words.count - 1]))
        } else {
            self.speak(self.NO_TEXT);
        }
    }
    
    func speakAllWords() {
        if let text = self.textDocumentProxy.documentContextBeforeInput {
            self.speak(self.ALL_WORDS)
            self.speak(text);
        } else {
            self.speak(self.NO_TEXT);
        }
    }
    
    func speak(text: String, ifUserDefaultsKeyIsFalse key: String) {
        let defaults = NSUserDefaults()
        if !defaults.boolForKey(key) {
            defaults.setBool(true, forKey: key)
            self.speak(text)
        }
    }
    
    // Fala um texto por TTS
    func speak(text: String) {
        let speechUtterance = AVSpeechUtterance(string: (text == " ")
            ? self.SPACE_CHARACTER_SPEECH
            : text)
        self.SPEECH_SYNTHESIZER.speakUtterance(speechUtterance)
    }
    
    
    
    
    // MARK: Outlets
    
    @IBOutlet weak var lblDisplayText: UILabel!
    
    
    
    
    // MARK: Actions
    
    // Tap com 1 dedo adiciona um ponto cheio e um ponto vazio à célula Braille
    @IBAction func gstTap_Touch1_action(sender: UITapGestureRecognizer) {
        if sender.state == .Ended {
            // O toque foi na metade esquerda ou direita da tela?
            if sender.locationOfTouch(0, inView: self.view).x < self.view.bounds.size.width / 2 {
                self.addDotsToBrailleCell(d1:1, d2:0)
            } else {
                self.addDotsToBrailleCell(d1:0, d2:1)
            }
        }
    }
    
    // Tap com 2 dedos adiciona dois pontos cheios à célula Braille
    @IBAction func gstTap_Touch2_action(sender: UITapGestureRecognizer) {
        if sender.state == .Ended {
            self.addDotsToBrailleCell(d1:1,d2:1)
        }
    }
    
    // Tap com 3 dedos avança para o próximo teclado
    @IBAction func gstTap_Touch3_action(sender: UITapGestureRecognizer) {
        if sender.state == .Ended {
            self.advanceToNextInputMode()
        }
    }
    
    // Swipe left cancela o preenchimento da célula ou apaga um caractere
    @IBAction func gstSwipe_Left_action(sender: UISwipeGestureRecognizer) {
        if sender.state == .Ended {
            self.setDisplayText("")
            if self.brailleRow == 0 {
                self.deleteOneCharacterFromDocument()
            } else {
                self.speak(self.CANCELLED_TYPING)
                self.resetBrailleCell()
            }
        }
    }
    
    // Swipe right adiciona dois pontos vazios à célula Braille
    @IBAction func gstSwipe_Right_action(sender: UISwipeGestureRecognizer) {
        if sender.state == .Ended {
            self.addDotsToBrailleCell(d1:0, d2:0)
        }
    }
    
    // Long press com 1 dedo
    @IBAction func gstLongPress_Touch1_action(sender: UILongPressGestureRecognizer) {
        if sender.state == .Began {
            self.speakLastWord()
        }
    }
    
    // Long press com 2 dedos
    @IBAction func gstLongPress_Touch2_action(sender: UILongPressGestureRecognizer) {
        if sender.state == .Began {
            self.speakAllWords()
        }
    }
}
