//
//  Alphabet.swift
//  KeyboardDemo
//
//  Created by João Marcelo on 07/10/15.
//  Copyright © 2015 João Marcelo Souza. All rights reserved.
//

struct BrailleCharacterMap {
    
    enum BrailleMode: String {
        case Letters = "Letras"
        case Numbers = "Números"
    }
    
    static func nextMode() -> BrailleMode {
        switch mode {
        case .Letters:
            mode = .Numbers
        case .Numbers:
            mode = .Letters
        }
        return mode
    }
    
    static func find(brailleCell: [Int], isNumber: Bool = false) -> String? {
        // Busca em pontuação
        if let char = punctuation[brailleCell] {
            return char;
        }
        
        // Busca em números ou letras
        if isNumber {
            return numbers[brailleCell]
        } else {
            return letters[brailleCell]
        }
    }
    
    static var mode = BrailleMode.Letters
    
    static let special:[String:[Int]] = [
        "capital":
            [0,0,0,1,0,1],
        "number":
            [0,0,1,1,1,1]
    ]
    
    static let letters = [
        [1,0,0,0,0,0]: "a",
        [1,1,0,0,0,0]: "b",
        [1,0,0,1,0,0]: "c",
        [1,0,0,1,1,0]: "d",
        [1,0,0,0,1,0]: "e",
        [1,1,0,1,0,0]: "f",
        [1,1,0,1,1,0]: "g",
        [1,1,0,0,1,0]: "h",
        [0,1,0,1,0,0]: "i",
        [0,1,0,1,1,0]: "j",
        
        [1,0,1,0,0,0]: "k",
        [1,1,1,0,0,0]: "l",
        [1,0,1,1,0,0]: "m",
        [1,0,1,1,1,0]: "n",
        [1,0,1,0,1,0]: "o",
        [1,1,1,1,0,0]: "p",
        [1,1,1,1,1,0]: "q",
        [1,1,1,0,1,0]: "r",
        [0,1,1,1,0,0]: "s",
        [0,1,1,1,1,0]: "t",
        
        [1,0,1,0,0,1]: "u",
        [1,1,1,0,0,1]: "v",
        [0,1,0,1,1,1]: "w",
        [1,0,1,1,0,1]: "x",
        [1,0,1,1,1,1]: "y",
        [1,0,1,0,1,1]: "z",
        
        [1,1,1,1,0,1]: "ç",
        
        [1,1,1,0,1,1]: "á",
        [1,1,0,1,0,1]: "à",
        [1,0,0,0,0,1]: "â",
        [0,0,1,1,1,0]: "ã",
        
        [1,1,1,1,1,1]: "é",
        [1,1,0,0,0,1]: "ê",
        
        [0,0,1,1,0,0]: "í",
        
        [0,0,1,1,0,1]: "ó",
        [1,0,0,1,1,1]: "ô",
        [0,1,0,1,0,1]: "õ",
        
        [0,1,1,1,1,1]: "ú",
    ]
    
    static let numbers = [
        [1,0,0,0,0,0]: "1",
        [1,1,0,0,0,0]: "2",
        [1,0,0,1,0,0]: "3",
        [1,0,0,1,1,0]: "4",
        [1,0,0,0,1,0]: "5",
        [1,1,0,1,0,0]: "6",
        [1,1,0,1,1,0]: "7",
        [1,1,0,0,1,0]: "8",
        [0,1,0,1,0,0]: "9",
        [0,1,0,1,1,0]: "0",
    ]
    
    static var punctuation = [
        [0,0,0,0,0,0]: " ",
        [0,1,0,0,0,0]: ",",
        [0,1,1,0,0,0]: ";",
        [0,1,0,0,1,0]: ":",
        [0,0,1,0,0,0]: ".",
        [0,1,0,0,0,1]: "?",
        [0,1,1,0,1,0]: "!",
        [0,0,1,0,0,1]: "-",
    ]
}