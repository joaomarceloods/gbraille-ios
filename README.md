# iBraille

O iBraille é uma adaptação do GBraille Keyboard para o iOS. É um projeto de pesquisa para desenvolver uma solução de teclado para dispositivos móveis (tablets e smartphones) acessível para deficientes visuais. Originalmente, a versão iOS também foi denominada "GBraille" e alguns arquivos permanecem com esta denominação.

## Projeto Xcode

O projeto foi desenvolvido no Xcode 7 no OS X El Capitan (10.11). O projeto é composto por duas partes:

* **Aplicação** (GBraille) - é a aplicação que empacota o teclado, contendo instruções e outras informações
* **Custom Keyboard Extension** (GBrailleKeyboard) - é o teclado de fato

## Abrindo no Xcode

Para abrir o projeto, clone o repositório em um diretório local na sua máquina e abra o arquivo **GBraille.xcodeproj** no Xcode.

O projeto está estruturado da seguinte maneira:

    GBraille (projeto Xcode)
    ├── GBraille (aplicação)
    │   └── ...
    ├── GBrailleKeyboard (Custom Keyboard Extension)
    │   └── ...
    └── Products (gerado pelo Xcode)
        └── ...

## Executando no iOS

A aplicação pode ser executada em um simulador ou dispositivo com iOS 8 ou superior:

1. Conecte o dispositivo no computador e selecione-o como destino no Xcode (**Product >> Destination >> [seu dispositivo]**)
2. Defina a Custom Keyboard Extension como Scheme de execução (**Product >> Scheme >> GBrailleKeyboard**)
3. Execute (**Product >> Run**)

Na primeira execução, será necessário adicionar o teclado nos ajustes do dispositivo e conceder *acesso total*:

1. **Ajustes >> Geral >> Teclado >> Teclados**
2. **Adicionar Novo Teclado... >> iBraille**
3. **Teclado iBraille - iBraille >> Permitir Acesso Total**

Para usar o teclado, ative qualquer campo de texto do sistema. Caso outro teclado apareça no lugar do iBraille, alterne até encontrá-lo.